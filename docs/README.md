
## summary
_june 28th, 2022_

currently tracking `106` groups across `155` relays & mirrors - _`56` currently online_

⏲ there have been `4` posts within the `last 24 hours`

🦈 there have been `211` posts within the `month of june`

🪐 there have been `928` posts within the `last 90 days`

🏚 there have been `1844` posts within the `year of 2022`

🦕 there have been `4130` posts `since the dawn of ransomwatch`

there are `51` custom parsers indexing posts

_`20` sites using v2 onion services are no longer indexed - [support.torproject.org](https://support.torproject.org/onionservices/v2-deprecation/)_

> see the project [README](https://github.com/joshhighet/ransomwatch#ransomwatch--) for backend technicals
